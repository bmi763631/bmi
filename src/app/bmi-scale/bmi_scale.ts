export default [
    {
      name: "wygłodzenie",
      scale_human: "< 16",
      scale: (n:number) => n<16,
      color: "rgb(8, 46, 121)",
    },
    {
      name: "wychudzenie",
      scale_human: "16 – 16,99 ",
      scale: (n:number) => n>=16 && n<17,
      color: "rgb(65, 105, 225)",
    },
    {
      name: "niedowaga",
      scale_human: "17 – 18,49 ",
      scale: (n:number) => n>=17 && n<18.5,
      color: "rgb(172, 225, 175)",
    },
    {
      name: "pożądana masa ciała",
      scale_human: "18,5 – 24,99 ",
      scale: (n:number) => n>=18.5 && n<25,
      color: "rgb(205, 235, 167)",
    },
    {
      name: "nadwaga",
      scale_human: "25 – 29,99",
      scale: (n:number) => n>=25 && n<30,
      color: "rgb(255, 255, 153)",
    },
    {
      name: "otyłość I stopnia",
      scale_human: "30 – 34,49",
      scale: (n:number) => n>=30 && n<35,
      color: "rgb(253, 228, 86)",
    },
    {
      name: "otyłość II stopnia",
      scale_human: "35 – 39,99",
      scale: (n:number) => n>=35 && n<40,
      color: "rgb(207, 41, 41)",
    },
    {
      name: "otyłość III stopnia",
      scale_human: "≥ 40",
      scale: (n:number) => n>=40,
      color: "rgb(128, 24, 24)",
    },
  ]