import { Component, OnInit, Input } from '@angular/core';

import bmi_scale from './bmi_scale'

@Component({
  selector: 'app-bmi-scale',
  templateUrl: './bmi-scale.component.html',
  styleUrls: ['./bmi-scale.component.css']
})
export class BmiScaleComponent implements OnInit {

  open_panel = -1

  skala_bmi = bmi_scale

  constructor() { }

  ngOnInit(): void {
  }

  @Input() 
  get bmi():number {
    return this._bmi
  }
  set bmi(bmi:number) {
    if (bmi < 0) return;
    this._bmi = bmi
    for (let i in this.skala_bmi) {
      let step = this.skala_bmi[i]
      if (step.scale(bmi)) {
        this.open_panel = parseInt(i)
      }
    }
  }

  private _bmi = -1;
}
