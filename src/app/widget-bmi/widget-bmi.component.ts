import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import calculate_bmi from '../bmi_calculator'

@Component({
  selector: 'app-widget-bmi',
  templateUrl: './widget-bmi.component.html',
  styleUrls: ['./widget-bmi.component.css']
})
export class WidgetBmiComponent implements OnInit {

  constructor() { }

  bmiForm = new FormGroup({
    weight: new FormControl('', Validators.required),
    height: new FormControl('', Validators.required),
  })

  calculatedBmi = -1;
  readableBmi = '??';

  ngOnInit(): void {
    this.bmiForm.valueChanges.subscribe(changes => {
      if (this.bmiForm.valid) {
        if (typeof(changes.weight) === 'number' && typeof(changes.height) === 'number')
          this.calculatedBmi = calculate_bmi(changes.weight, changes.height);
          if (this.calculatedBmi > 99 || this.calculatedBmi <= 0) {
            this.readableBmi = "??";
          } else {
            this.readableBmi = (this.calculatedBmi).toString().substring(0, 4)
          }
          
      }
    })
  }

}
