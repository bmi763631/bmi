import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetBmiComponent } from './widget-bmi.component';

describe('WidgetBmiComponent', () => {
  let component: WidgetBmiComponent;
  let fixture: ComponentFixture<WidgetBmiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetBmiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WidgetBmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
